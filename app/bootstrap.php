<?php 

require '../vendor/autoload.php';

date_default_timezone_set("Asia/Bangkok");

// $objPHPExcel = PHPExcel_IOFactory::load("../public/test.xlsx");

// $objPHPExcel->setActiveSheetIndex(0)
//                             ->setCellValue('A2', "No")
//                             ->setCellValue('B2', "Name")
//                             ->setCellValue('C2', "Email")
//                             ->setCellValue('D2', "Phone")
//                             ->setCellValue('E2', "Address");

// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
// $objWriter->save('keywords.xlsx'); 

$target ='../public/';
$fileType = 'Excel2007';   
$InputFileName = $target.'test.xlsx';   
$OutputFileName =  $target . 'test2.xlsx';

 //Read the file (including chart template) 
$objReader = PHPExcel_IOFactory::createReader($fileType); 
$objReader->setIncludeCharts(TRUE);
$objPHPExcel = $objReader->load($InputFileName); 

 //Change the file 


$objPHPExcel->setActiveSheetIndex(0)
// Add data
            ->setCellValue('C6','Ingenieria en sistemas computacionales' );

 //Write the file (including chart)

PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType); 
$objWriter->setIncludeCharts(TRUE);
$objWriter->save($OutputFileName);

echo "hello";